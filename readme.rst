etasl_iohandler_jointstate
===========================

Introduction
------------

This package provides additional I/O-handlers for the etasl_rtt component.
The I/O-handlers are all related to ROS JointState messages.


You can load this component as follows (oro-script):

.. code-block:: lua

    import("etasl_iohandler_jointstate")
    loadComponent("io","Etasl_IOHandler_Jointstate")


This component is used as a *factory*.
It is not necessary to start this component or to associate an activity with this component.
Once the I/O-handler is created using one of this component's operations, the component is not
needed any more.


I/O-handlers
------------

.. code-block:: lua


     add_controller_jointstate_inputport( 
        string const& etasl_comp_name, string const& portname, string const& portdocstring, strings const& jointnames 
       ) : bool
       adds an IOHandler that read measured jointstate position values from an outputport
       etasl_comp_name : name of the eTaSL component to add the port to
       portname : name of the port
       portdocstring : documentation for the port
       jointnames : list of jointnames describing the contents of the inputport

     add_controller_jointstate_output( string const& etasl_comp_name, string const& portname, string const& portdocstring, strings const& jointnames ) : bool
       adds an IOHandler that puts the joint POSITIONS on an outputport using JointState::Messages
       etasl_comp_name : name of the eTaSL component to add the port to
       portname : name of the port
       portdocstring : documentation for the port
       jointnames : list of jointnames describing the contents of the outputport


License
-------

GNU LGPL v3, see LICENSE

Author
------


Erwin Aertbeliën, 2016.

